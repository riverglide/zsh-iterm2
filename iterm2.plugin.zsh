# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Reasons to not decorate the prompt - guard clauses
if [[ ! -o interactive ]]; then return; fi
if [[ "${ITERM_ENABLE_SHELL_INTEGRATION_WITH_TMUX-}""$TERM" = "screen" ]]; then return; fi
if [[ "${ITERM_SHELL_INTEGRATION_INSTALLED-}" != "" ]]; then return; fi
if [[ "$TERM" = linux ]]; then return; fi
if [[ "$TERM" = dumb ]]; then return; fi

# If we get to here, then we didn't have a reason to not decorate
ITERM_SHELL_INTEGRATION_INSTALLED=Yes

# FinalTerm commands as implemented/extended by iTerm2 https://www.iterm2.com/documentation-escape-codes.html
_iterm_ft_cmd() { printf $'\e]133;%s\x07' $@ }

# iTerm2 extended Operating System Command (OSC)
_iterm_si_cmd() { printf $'\e]1337;%s=%s\x07' $@ }

mark_start_of_prompt()                         { _iterm_ft_cmd "A" }
mark_end_of_prompt()                           { _iterm_ft_cmd "B" }
mark_start_of_command_output()                 { _iterm_ft_cmd "C" }
mark_end_of_command_output_and_record_status() { _iterm_ft_cmd "D;$STATUS" }

# legacy function name because it's referenced in powerlevel9k
iterm2_prompt_mark() { mark_start_of_prompt }

iterm2_print_state_data() {
  _iterm_si_cmd "RemoteHost" "${USER}@${iterm2_hostname-}"
  _iterm_si_cmd "CurrentDir" "$PWD"
  iterm2_print_user_vars
}

iterm2_set_user_var() {
  _iterm_si_cmd "SetUserVar" $(printf "%s=%s" "$1" $(printf "%s" "$2" | base64 | tr -d '\n'))
}

# Users can write their own version of this method. It should call
# iterm2_set_user_var but not produce any other output.
# e.g., iterm2_set_user_var currentDirectory $PWD
# Accessible in iTerm2 (in a badge now, elsewhere in the future) as
# \(user.currentDirectory).
whence -v iterm2_print_user_vars > /dev/null 2>&1
if [ $? -ne 0 ]; then
  iterm2_print_user_vars() {
      true
  }
fi


# There are three possible paths in life.
#
# 1) A command is entered at the prompt and you press return.
#    The following steps happen:
#    * iterm2_preexec is invoked
#      * PS1 is set to ITERM2_UNDECORATED_PROMPT
#      * ITERM2_COMMAND_EXECUTING is set to 1
#    * The command executes (possibly reading or modifying PS1)
#    * iterm2_precmd is invoked
#      * ITERM2_UNDECORATED_PROMPT is set to PS1 (as modified by command execution)
#      * PS1 gets our escape sequences added to it
#    * zsh displays your prompt
#    * You start entering a command
#
# 2) You press ^C while entering a command at the prompt.
#    The following steps happen:
#    * (iterm2_preexec is NOT invoked)
#    * iterm2_precmd is invoked
#      * mark_start_of_command_output is called since we detected that iterm2_preexec was not run
#      * (ITERM2_UNDECORATED_PROMPT and PS1 are not messed with, since PS1 already has our escape
#        sequences and ITERM2_UNDECORATED_PROMPT already has PS1's original value)
#    * zsh displays your prompt
#    * You start entering a command
#
# 3) A new shell is born.
#    * PS1 has some initial value, either zsh's default or a value set before this script is sourced.
#    * iterm2_precmd is invoked
#      * ITERM2_COMMAND_EXECUTING is initialized to 1
#      * ITERM2_UNDECORATED_PROMPT is set to the initial value of PS1
#      * PS1 gets our escape sequences added to it
#    * Your prompt is shown and you may begin entering a command.
#
# Invariants:
# * ITERM2_COMMAND_EXECUTING is 1 during and just after command execution, and "" while the prompt is
#   shown and until you enter a command and press return.
# * PS1 does not have our escape sequences during command execution
# * After the command executes but before a new one begins, PS1 has escape sequences and
#   ITERM2_UNDECORATED_PROMPT has PS1's original value.
#
# What this boils down to is we want to have PS1 set with our marks while we're displaying a prompt
# (e.g. first display of prompt, pressing return with no command, pressing ^C to interrupt),
# but we want to reset it to the original when a command is executing so that commands don't get
# confused by the special command characters

iterm2_decorate_prompt() {
  # This should be a raw PS1 without iTerm2's stuff. It could be changed during command
  # execution.
  ITERM2_UNDECORATED_PROMPT="$PS1"
  ITERM2_COMMAND_EXECUTING=""

  # Add our escape sequences just before the prompt is shown.
  # Use ITERM2_SQUELCH_MARK for people who can't modify PS1 directly, like powerlevel9k users.
  # This is gross but I had a heck of a time writing a correct if statetment for zsh 5.0.2.
  local PREFIX="$(mark_start_of_prompt)"
  local SUFFIX="$(mark_end_of_prompt)"
  if [[ $PS1 == *"$PREFIX"* ]]; then PREFIX=""; fi # The prompt already contains the start mark
  if [[ "${ITERM2_SQUELCH_MARK-}" != "" ]]; then PREFIX=""; fi

  PS1="$PREFIX$PS1$SUFFIX"
}

iterm2_precmd() {
  local STATUS="$?"
  if [ -z "${ITERM2_COMMAND_EXECUTING-}" ]; then
    # You pressed ^C while entering a command (iterm2_preexec did not run)
    mark_start_of_command_output
  fi

  mark_end_of_command_output_and_record_status "$STATUS"
  iterm2_print_state_data

  if [ -n "$ITERM2_COMMAND_EXECUTING" ]; then
    iterm2_decorate_prompt
  fi
}

# This is not run if you press ^C while entering a command.
iterm2_preexec() {
  # Set PS1 back to its raw value prior to executing the command.
  PS1="$ITERM2_UNDECORATED_PROMPT"
  ITERM2_COMMAND_EXECUTING="1"
  mark_start_of_command_output
}

# If hostname -f is slow on your system, set iterm2_hostname prior to sourcing this script.
[[ -z "${iterm2_hostname-}" ]] && iterm2_hostname=`hostname -f 2>/dev/null`
# some flavors of BSD (i.e. NetBSD and OpenBSD) don't have the -f option
if [ $? -ne 0 ]; then
  iterm2_hostname=`hostname`
fi

autoload -Uz add-zsh-hook
add-zsh-hook preexec iterm2_preexec
add-zsh-hook precmd  iterm2_precmd

iterm2_print_state_data
_iterm_si_cmd "ShellIntegrationVersion" "10;shell=zsh"
