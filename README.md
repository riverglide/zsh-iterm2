# zsh-iterm2

Provides the iterm2 terminal integration as a `zplug` plugin, and the iterm2 utilities as `zplug` commands.

Add the following to your `.zshrc` to enable prompt marking
```
zplug "RiverGlide/zsh-iterm2", from:gitlab
```

Add the following to your `.zshrc` to add the iterm2 utilities to the command path
```
zplug "RiverGlide/zsh-iterm2", from:gitlab, as:command, use:"utilities/*"
```

or only add the utilities you want with
```
zplug "RiverGlide/zsh-iterm2", from:gitlab, as:command, use:"utilities/{imgls,imgcat}"
```
